import { RouterModule, Route } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductSetupComponent } from './product-setup/product-setup.component';

export const ProductFutureState: Route = {
  path: 'product',
  loadChildren: () => import('./product-list/product-list.module').then(mod => mod.ProductListModule)
};

const states = [{
  path: '',
  children: [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  }, {
    path: 'list',
    component: ProductListComponent
  }, {
    path: 'create',
    component: ProductSetupComponent
  }, {
    path: 'edit/:id',
    component: ProductSetupComponent
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(states)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule { }
