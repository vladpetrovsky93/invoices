import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list.component';
import { ProductRoutingModule } from '../product-routing.module';
import { ProductSetupModule } from '../product-setup/product-setup.module';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    ProductRoutingModule,
    ProductSetupModule,
  ],
  declarations: [
    ProductListComponent,
  ],
})
export class ProductListModule {

}
