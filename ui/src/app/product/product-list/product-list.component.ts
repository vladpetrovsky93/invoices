import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ProductSetup, ProductDetails } from '../product.models';
import { ProductService } from '../product.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns: Array<keyof ProductSetup | 'edit'> = ['name', 'price', 'edit'];
  dataSource: MatTableDataSource<ProductSetup>;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.prepareTable();
  }

  private prepareTable() {
    this.productService.getAll().subscribe(products => {
      this.dataSource = new MatTableDataSource(products);
      this.dataSource.sort = this.sort;
    });
  }

  removeProduct(product: ProductDetails) {
    this.productService.delete(product.id).subscribe(() => {
      // The simplest solution. Otherwise, we can push updated value into existing array for avoiding an additional API request
      this.prepareTable();
    });
  }
}
