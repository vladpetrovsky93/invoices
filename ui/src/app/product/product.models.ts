export interface ProductSetup {
  name: string;
  price: number;
}

export interface ProductDetails extends ProductSetup {
  id: number;
}
