import { Component, OnInit, Optional } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { ProductService } from '../product.service';
import { ProductDetails } from '../product.models';

@Component({
  selector: 'product-setup',
  templateUrl: './product-setup.component.html',
  styleUrls: ['./product-setup.component.scss']
})
export class ProductSetupComponent implements OnInit {

  productForm = new FormGroup({
    name: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required)
  });

  productId: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productService: ProductService,
              @Optional() public dialogRef: MatDialogRef<ProductSetupComponent>) { }

  ngOnInit() {
    this.productId = this.route.snapshot.params.id;
    if (this.productId) {
      this.productService.getById(this.productId).subscribe(product => {
        this.fillForm(product);
      });
    }
  }

  private fillForm(product: ProductDetails) {
    this.productForm.patchValue({
      name: product.name,
      price: product.price
    });
  }

  save() {
    if (this.productForm.invalid) return;

    const productSetup$ = this.productId ?
      this.productService.update(this.productId, this.productForm.value) :
      this.productService.create(this.productForm.value);

    productSetup$.subscribe(product => {
      if (this.dialogRef) this.dialogRef.close(product);
      else this.router.navigateByUrl('/product');
    });
  }

  get name(): FormControl {
    return this.productForm.get('name') as FormControl;
  }

  get price(): FormControl {
    return this.productForm.get('price') as FormControl;
  }
}
