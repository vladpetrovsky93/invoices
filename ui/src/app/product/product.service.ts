import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductDetails, ProductSetup } from './product.models';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl = `${environment.apiUrl}/products`;

  constructor(private httpClent: HttpClient) { }

  getAll(): Observable<ProductDetails[]> {
    return this.httpClent.get<ProductDetails[]>(this.baseUrl);
  }

  getById(id: number): Observable<ProductDetails> {
    return this.httpClent.get<ProductDetails>(`${this.baseUrl}/${id}`);
  }

  create(customer: ProductSetup): Observable<ProductDetails> {
    return this.httpClent.post<ProductDetails>(this.baseUrl, customer);
  }

  update(id: number, customer: ProductSetup): Observable<ProductDetails> {
    return this.httpClent.put<ProductDetails>(`${this.baseUrl}/${id}`, customer);
  }

  delete(id: number): Observable<void> {
    return this.httpClent.delete<void>(`${this.baseUrl}/${id}`);
  }

}
