import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerFutureState } from './customer/customer-routing.module';
import { ProductFutureState } from './product/product-routing.module';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'invoice' },
  CustomerFutureState,
  ProductFutureState,
  { path: '**', redirectTo: 'invoice' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
