import { CustomerSetupModule } from './../customer/customer-setup/customer-setup.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ProductSetupModule } from '../product/product-setup/product-setup.module';
import { InvoiceSetupComponent } from './invoice-setup/invoice-setup.component';

@NgModule({
  declarations: [
    InvoiceListComponent,
    InvoiceSetupComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InvoiceRoutingModule,
    MatTableModule,
    MatButtonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSortModule,
    MatDialogModule,
    MatSnackBarModule,
    CustomerSetupModule,
    ProductSetupModule,
  ]
})
export class InvoiceModule { }
