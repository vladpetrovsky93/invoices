import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { InvoiceSetupComponent } from './invoice-setup/invoice-setup.component';

const states = [{
  path: 'invoice',
  children: [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  }, {
    path: 'list',
    component: InvoiceListComponent
  }, {
    path: 'create',
    component: InvoiceSetupComponent
  }, {
    path: 'edit/:id',
    component: InvoiceSetupComponent
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(states)
  ],
  exports: [
    RouterModule
  ],
})
export class InvoiceRoutingModule { }
