import { CustomerDetails } from './../../customer/customer.models';
import { InvoiceService } from './../invoice.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Invoice } from '../invoice.models';
import { CustomerService } from 'src/app/customer/customer.service';
import { InvoiceDetails } from '../invoice-setup/invoice-setup.models';

@Component({
  selector: 'invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  invoices: Invoice[];

  displayedColumns: Array<keyof Invoice | 'edit'> = ['customer_id', 'discount', 'total', 'edit'];
  dataSource: MatTableDataSource<Invoice>;

  customerIdToCustomer = new Map<number, CustomerDetails>();

  constructor(private invoiceService: InvoiceService,
              private customerService: CustomerService) {}

  ngOnInit() {
    this.prepareTable();

    this.customerService.getAll().subscribe(customers => {
      this.customerIdToCustomer = customers.reduce(
        (acc, customer) => acc.set(customer.id, customer),
        new Map()
      );
    });
  }

  private prepareTable() {
    this.invoiceService.getAll().subscribe(invoices => {
      this.invoices = invoices;
      this.dataSource = new MatTableDataSource(invoices);
      this.dataSource.sort = this.sort;
    });
  }

  removeInvoice(invoice: InvoiceDetails) {
    this.invoiceService.delete(invoice.id).subscribe(() => {
      // The simplest solution. Otherwise, we can push updated value into existing array for avoiding an additional API request
      this.prepareTable();
    });
  }

}
