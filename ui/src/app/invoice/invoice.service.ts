import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Invoice } from './invoice.models';
import { InvoiceSetup, InvoiceDetails, InvoiceItemSetup, InvoiceItemDetails } from './invoice-setup/invoice-setup.models';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  private baseUrl = `${environment.apiUrl}/invoices`;

  constructor(private httpClent: HttpClient) {}

  getAll(): Observable<Invoice[]> {
    return this.httpClent.get<Invoice[]>(this.baseUrl);
  }

  getById(id: number): Observable<Invoice> {
    return this.httpClent.get<Invoice>(`${this.baseUrl}/${id}`);
  }

  create(invoice: InvoiceSetup): Observable<InvoiceDetails> {
    return this.httpClent.post<InvoiceDetails>(this.baseUrl, invoice);
  }

  update(id: number, invoice: InvoiceSetup): Observable<InvoiceDetails> {
    return this.httpClent.put<InvoiceDetails>(`${this.baseUrl}/${id}`, invoice);
  }

  delete(id: number): Observable<void> {
    return this.httpClent.delete<void>(`${this.baseUrl}/${id}`);
  }

  getItems(id: number): Observable<InvoiceItemDetails[]> {
    return this.httpClent.get<InvoiceItemDetails[]>(`${this.baseUrl}/${id}/items`);
  }

  setItems(id: number, invoiceItems: InvoiceItemSetup[]): Observable<void> {
    return this.httpClent.post<void>(`${this.baseUrl}/${id}/items`, invoiceItems);
  }

}
