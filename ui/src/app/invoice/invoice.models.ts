export interface Invoice {
  id: number;
  customer_id: number;
  discount: number;
  total: number;
}
