import { CustomerSetupComponent } from './../../customer/customer-setup/customer-setup.component';
import { CustomerDetails } from './../../customer/customer.models';
import { CustomerService } from './../../customer/customer.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil, debounceTime, filter } from 'rxjs/operators';
import { InvoiceService } from '../invoice.service';
import { MatDialog } from '@angular/material/dialog';
import { ProductService } from 'src/app/product/product.service';
import { ProductDetails } from 'src/app/product/product.models';
import { ProductSetupComponent } from 'src/app/product/product-setup/product-setup.component';
import { InvoiceCreatorFormData, InvoiceDetails, InvoiceItemDetails } from './invoice-setup.models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'invoice-setup',
  templateUrl: './invoice-setup.component.html',
  styleUrls: ['./invoice-setup.component.scss']
})
export class InvoiceSetupComponent implements OnInit, OnDestroy {

  customers: CustomerDetails[] = [];
  products: ProductDetails[] = [];

  productIdToProduct = new Map<number, ProductDetails>();

  invoiceForm = new FormGroup({
    customer: new FormControl('', Validators.required),
    products: new FormArray([this.buildProductControl()]),
    discount: new FormControl(0, [Validators.min(0), Validators.max(100)])
  });

  invoiceId: number;

  total = 0;
  private destroy$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private invoiceService: InvoiceService,
              private customerService: CustomerService,
              private productService: ProductService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) {

    const params = route.snapshot.params;
    if (params.id) this.invoiceId = params.id;

  }

  ngOnInit() {
    if (this.invoiceId) {
      forkJoin(
        this.invoiceService.getById(this.invoiceId),
        this.invoiceService.getItems(this.invoiceId)
      ).subscribe(data => {
        this.fillForm(data);
        this.initFormListeners();
      });
    } else {
      this.initFormListeners();
    }

    this.customerService.getAll().subscribe(customers => {
      this.customers = customers;
    });
    this.productService.getAll().subscribe(products => {
      this.products = products;
      this.productIdToProduct = products.reduce(
        (acc, product) => acc.set(product.id, product),
        new Map()
      );
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  private fillForm([invoice, items]: [InvoiceDetails, InvoiceItemDetails[]]) {
    this.invoiceForm.patchValue({
      customer: invoice.customer_id,
      discount: invoice.discount
    });

    const productsControls = items.map(item => this.buildProductControl(item.product_id, item.quantity));
    this.invoiceForm.setControl('products', new FormArray(productsControls));

    this.total = invoice.total;
  }

  private initFormListeners(): void {
    const valueChanges$ = this.invoiceForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      filter(_ => this.invoiceForm.valid)
    );

    valueChanges$.subscribe(formValue => this.total = this.calcTotal(formValue));
    valueChanges$.pipe(debounceTime(3000)).subscribe(_ => this.saveInvoice());
  }

  private calcTotal(formValue: InvoiceCreatorFormData) {
    const fullPrice = formValue.products.reduce((sum, item) => {
      const product = this.productIdToProduct.get(item.product);
      const price = product ? product.price : 0;
      const quantity = item.quantity || 0;
      return sum + price * quantity;
    }, 0);
    const discount = (100 - formValue.discount) /  100;
    const finalPriceStr = (fullPrice * discount).toFixed(2);
    return parseFloat(finalPriceStr);
  }

  createCustomer() {
    const dialogRef = this.dialog.open<CustomerSetupComponent, void, CustomerDetails>(CustomerSetupComponent);

    dialogRef.afterClosed()
      .pipe(filter(customer => !!customer))
      .subscribe(customer => {
        this.customers.push(customer);
        this.customerControl.setValue(customer.id);
      });
  }

  createProduct() {
    const dialogRef = this.dialog.open<ProductSetupComponent, void, ProductDetails>(ProductSetupComponent);

    dialogRef.afterClosed()
      .pipe(filter(product => !!product))
      .subscribe(product => {
        this.products.push(product);
        this.productIdToProduct.set(product.id, product);
        this.selectProduct(product);
      });
  }

  selectProduct(product?: ProductDetails): void {
    this.productsControl.push(this.buildProductControl(product ? product.id : null));
  }

  removeProduct(i: number): void {
    this.productsControl.removeAt(i);
  }

  private buildProductControl(productId?: number, quantity?: number): FormGroup {
    const productControl = new FormControl(productId || '', Validators.required);
    const quantityControl = new FormControl({value: quantity || '', disabled: !productId}, [Validators.required, Validators.min(0)]);
    productControl.valueChanges.subscribe(updatedProduct => {
      updatedProduct ? quantityControl.enable() : quantityControl.disable();
    });
    return new FormGroup({
      product: productControl,
      quantity: quantityControl
    });
  }

  private saveInvoice(): void {
    const invoiceFormData = this.invoiceForm.value as InvoiceCreatorFormData;
    const invoice = {
      customer_id: invoiceFormData.customer,
      discount: invoiceFormData.discount,
      total: this.total
    };

    const setupInvoice$ = this.invoiceId ?
      this.invoiceService.update(this.invoiceId, invoice) :
      this.invoiceService.create(invoice);

    setupInvoice$.subscribe(createdInvoice => {
      this.invoiceId = createdInvoice.id;

      const invoiceItems = invoiceFormData.products.map(product => ({
        invoice_id: createdInvoice.id,
        product_id: product.product,
        quantity: product.quantity
      }));
      this.invoiceService.setItems(createdInvoice.id, invoiceItems).subscribe(() => {
        this.snackBar.open('Invoice is updated', null, {duration: 3000});
      });
    });
  }

  get customerControl(): FormControl {
    return this.invoiceForm.get('customer') as FormControl;
  }

  get productsControl(): FormArray {
    return this.invoiceForm.get('products') as FormArray;
  }

  get discountControl(): FormControl {
    return this.invoiceForm.get('discount') as FormControl;
  }

}
