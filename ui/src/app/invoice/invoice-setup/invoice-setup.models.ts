export interface InvoiceSetup {
  customer_id: number;
  discount: number;
  total: number;
}

export interface InvoiceDetails extends InvoiceSetup {
  id: number;
}

export interface InvoiceItemSetup {
  invoice_id: number;
  product_id: number;
  quantity: number;
}

export interface InvoiceItemDetails extends InvoiceItemSetup {
  id: number;
}

export interface InvoiceCreatorFormData {
  customer: number;
  products: InvoiceCreatorProductFormData[];
  discount: number;
}

export interface InvoiceCreatorProductFormData {
  product: number;
  quantity: number;
}


