import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerDetails, CustomerSetup } from './customer.models';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private baseUrl = `${environment.apiUrl}/customers`;

  constructor(private httpClent: HttpClient) { }

  getAll(): Observable<CustomerDetails[]> {
    return this.httpClent.get<CustomerDetails[]>(this.baseUrl);
  }

  getById(id: number): Observable<CustomerDetails> {
    return this.httpClent.get<CustomerDetails>(`${this.baseUrl}/${id}`);
  }

  create(customer: CustomerSetup): Observable<CustomerDetails> {
    return this.httpClent.post<CustomerDetails>(this.baseUrl, customer);
  }

  update(id: number, customer: CustomerSetup): Observable<CustomerDetails> {
    return this.httpClent.put<CustomerDetails>(`${this.baseUrl}/${id}`, customer);
  }

  delete(id: number): Observable<void> {
    return this.httpClent.delete<void>(`${this.baseUrl}/${id}`);
  }

}
