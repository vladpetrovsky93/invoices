import { RouterModule, Route } from '@angular/router';
import { NgModule } from '@angular/core';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerSetupComponent } from './customer-setup/customer-setup.component';

export const CustomerFutureState: Route = {
  path: 'customer',
  loadChildren: () => import('./customer-list/customer-list.module').then(mod => mod.CustomerListModule)
};

const states = [{
  path: '',
  children: [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  }, {
    path: 'list',
    component: CustomerListComponent
  }, {
    path: 'create',
    component: CustomerSetupComponent
  }, {
    path: 'edit/:id',
    component: CustomerSetupComponent
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(states)
  ],
  exports: [
    RouterModule
  ]
})
export class CustomerRoutingModule { }
