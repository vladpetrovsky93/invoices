import { CustomerDetails } from './../customer.models';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CustomerService } from '../customer.service';
import { CustomerSetup } from '../customer.models';

@Component({
  selector: 'customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns: Array<keyof CustomerSetup | 'edit'> = ['name', 'address', 'phone', 'edit'];
  dataSource: MatTableDataSource<CustomerSetup>;

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.prepareTable();
  }

  private prepareTable() {
    this.customerService.getAll().subscribe(customers => {
      this.dataSource = new MatTableDataSource(customers);
      this.dataSource.sort = this.sort;
    });
  }

  removeCustomer(customer: CustomerDetails) {
    this.customerService.delete(customer.id).subscribe(() => {
      // The simplest solution. Otherwise, we can push updated value into existing array for avoiding an additional API request
      this.prepareTable();
    });
  }
}
