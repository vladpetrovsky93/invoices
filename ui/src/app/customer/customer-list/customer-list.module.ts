import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from '../customer-routing.module';
import { CustomerListComponent } from './customer-list.component';
import { CustomerSetupModule } from '../customer-setup/customer-setup.module';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    CustomerRoutingModule,
    CustomerSetupModule,
  ],
  declarations: [
    CustomerListComponent,
  ],
})
export class CustomerListModule {

}
