export interface CustomerSetup {
  name: string;
  address: string;
  phone: string;
}

export interface CustomerDetails extends CustomerSetup {
  id: number;
}
