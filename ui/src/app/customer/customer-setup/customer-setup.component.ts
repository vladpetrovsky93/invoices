import { CustomerDetails } from './../customer.models';
import { Component, OnInit, Optional } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'customer-setup',
  templateUrl: './customer-setup.component.html',
  styleUrls: ['./customer-setup.component.scss']
})
export class CustomerSetupComponent implements OnInit {

  customerForm = new FormGroup({
    name: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required)
  });

  customerId: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private customerService: CustomerService,
              @Optional() public dialogRef: MatDialogRef<CustomerSetupComponent>) { }

  ngOnInit() {
    this.customerId = this.route.snapshot.params.id;
    if (this.customerId) {
      this.customerService.getById(this.customerId).subscribe(customer => {
        this.fillForm(customer);
      });
    }
  }

  private fillForm(customer: CustomerDetails) {
    this.customerForm.patchValue({
      name: customer.name,
      address: customer.address,
      phone: customer.phone,
    });
  }

  save() {
    if (this.customerForm.invalid) return;

    const customerSetup$ = this.customerId ?
      this.customerService.update(this.customerId, this.customerForm.value) :
      this.customerService.create(this.customerForm.value);

    customerSetup$.subscribe(customer => {
      if (this.dialogRef) this.dialogRef.close(customer);
      else this.router.navigateByUrl('/customer');
    });
  }

  get name(): FormControl {
    return this.customerForm.get('name') as FormControl;
  }

  get address(): FormControl {
    return this.customerForm.get('address') as FormControl;
  }

  get phone(): FormControl {
    return this.customerForm.get('phone') as FormControl;
  }

}
