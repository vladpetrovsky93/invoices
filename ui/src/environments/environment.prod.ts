import { Environment } from './environment.models';

export const environment: Environment = {
  production: true,
  apiUrl: '/api',
};
