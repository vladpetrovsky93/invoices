# Getting Started

###### Install UI's npm dependencies
`npm install`

###### Run UI locally
`ng serve` <br />
Navigate to `http://localhost:4200/`. Please, keep in mind that server should be launched too.

###### Build ui
`ng build` <br />
The build artifacts will be stored in the `../public` directory and consumed by server. <br />
Navigate to `http://localhost:8000/`. Please, keep in mind that server should be launched too.
